import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';
import Counter from 'components/Counter';
import { render, fireEvent } from '@testing-library/react';

describe('Counter tests', () => {
  it('Counter match snap', () => {
    const {asFragment} = render(
      <Provider store={store}>
        <Counter />
      </Provider>
    );

    expect(asFragment()).toMatchSnapshot();
  });

  it('Counter check clicks', () => {
    const {getByTestId, getByText} = render(
      <Provider store={store}>
        <Counter />
      </Provider>
    );

    const increaseBtn = getByText(/\+/);
    const decreaseBtn = getByText(/\-/);
    const clearBtn = getByText(/clear/);
    const currentCountId = "current-count"
    expect(getByTestId(currentCountId).textContent).toBe("0");

    fireEvent.click(increaseBtn);
    expect(getByTestId(currentCountId).textContent).toBe("1");

    fireEvent.click(decreaseBtn);
    fireEvent.click(decreaseBtn);
    expect(getByTestId(currentCountId).textContent).toBe("-1");

    fireEvent.click(clearBtn);
    expect(getByTestId(currentCountId).textContent).toBe("0");
  });
});
