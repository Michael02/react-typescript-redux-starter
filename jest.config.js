module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.{ts,tsx}",
    "!src/types/*.ts",
    "!src/store/*/{index,constants}.ts",
    "!src/{App,index}.tsx",
    "!**/node_modules/**",
  ],
  coverageDirectory: "<rootDir>/coverage",
  globals: {
    __DEV__: true,
  },
  moduleNameMapper: {
    "^components(.*)$": "<rootDir>/src/components$1",
    "^navigation(.*)$": "<rootDir>/src/navigation$1",
    "^pages(.*)$": "<rootDir>/src/pages$1",
    "^store(.*)$": "<rootDir>/src/store$1",
    "^styles(.*)$": "<rootDir>/src/styles$1",
    "^types(.*)$": "<rootDir>/src/types$1",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/test/__mocks__/fileMock.js",
    "\\.(le|s?c)ss$": "<rootDir>/test/__mocks__/styleMock.js",
  },
  verbose: true,
};
